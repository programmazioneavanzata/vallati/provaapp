/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package it.unipi.primaapp;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Carlo Vallati
 */
public class PrimaApp {
    private static final Logger logger = LogManager.getLogger(PrimaApp.class);

    public static int somma(int a, int b){
        return a+b;
    }
    
    public static void main(String[] args) throws Exception {
        int i=0;
        
        logger.warn("warning");
        logger.error("errore");
        
        System.out.println("Hello World!"); 
        
        i++;
        
        System.out.println("Hello World!!!!!!");
        
        i=i+3;
        
        System.out.println("Hello World!");
        
        CloseableHttpClient httpClient = HttpClients.createDefault();
        
        HttpGet request = new HttpGet("https://www.google.com");
        
        CloseableHttpResponse response = httpClient.execute(request);
        
        HttpEntity entity = response.getEntity();
        
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }

    }
}
